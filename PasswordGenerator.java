import java.util.Random;

public class PasswordGenerator {
    private int length;

    //Constructor
    public PasswordGenerator(int length){
        this.length = length;
    }

    //Weak Password
    public String generateWeakPassword(){
        Random rand = new Random();
        String weak = "abcdefghijklmnopqrstuvwxyz";
        String password = "";
        char genLetter;

        for(int i = 0; i < this.length; i++){
            genLetter = weak.charAt(rand.nextInt(25));

            password = password + genLetter;
        }

        System.out.println(password);

        return password;
    }

    //Strong Password
    public String generateStrongPassword(){
        Random rand = new Random();
        String strong = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String password = "";
        char genLetter; 

        for(int i = 0; i < (this.length*2); i++){
            genLetter = strong.charAt(rand.nextInt(61));

            password = password + genLetter;
        }

        System.out.println(password);
        return password;
    }

    //Get Method
    public int getLength(){
        return this.length;
    }

}
