public class Login {
    
    private User[] userArray; 

    public Login(User[] userArray){
        this.userArray = userArray;
    }

    public User[] getUserArray(){
        return this.userArray;
    }

    public boolean validate(String username, String password, boolean admin) {
        for (int i = 0; i < userArray.length; i++) {
            
            if (userArray[i].getUsername().equals(username) && userArray[i].getPassword().equals(password) && admin == false){
                return true;
            }
            else if (userArray[i].getUsername().equals(username) && userArray[i].getPassword().equals(password) && admin == true && userArray[i].getUserType() == UserType.ADMIN){
                return true;
            }
            //continue here
        }
        return false;
    }
}