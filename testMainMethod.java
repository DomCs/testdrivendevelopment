public class testMainMethod {
    public static void main(String[] args) {
        PasswordGenerator testing = new PasswordGenerator(6);

        testing.generateWeakPassword();
        testing.generateStrongPassword();
    }
}
