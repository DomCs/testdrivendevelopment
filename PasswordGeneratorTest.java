import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PasswordGeneratorTest {
    
    @Test
    public void ConsturctorTest(){
        PasswordGenerator passGen = new PasswordGenerator(6);
        assertEquals(passGen.getLength(), 6);
    }

    @Test
    public void generateWeakPasswordTest(){
        PasswordGenerator passGen = new PasswordGenerator(6);
        assertEquals(passGen.generateWeakPassword().length(), 6);
    }

    @Test
    public void generateStrongPasswordTest(){
        PasswordGenerator passGen = new PasswordGenerator(6);
        assertEquals(passGen.generateStrongPassword().length(), 12);
    }

    // done!
}
