import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class TestLogin {
    
    @Test
    public void testConstructor(){
        User[] testArray = new User[3];
        User[] returnedUser = new User[3];


        UserType admin = UserType.ADMIN;    
        User userOne = new User("John","Hey", admin);

        testArray[1] = userOne;

        Login testing = new Login(testArray);


        returnedUser = testing.getUserArray();


        assertEquals(testArray[1], returnedUser[1]);
    }




    //Testing if the user is an admin and pass/user is the same.

    @Test

    public void testValidateTrue(){

        UserType admin = UserType.ADMIN;
        UserType regular = UserType.REGULAR;

        User someone = new User("Hey", "There", admin);
        User someoneB = new User("Hey", "There", regular);
        User someoneC = new User("Hey", "Hey", admin);

        User[] testArray = new User[3];
        testArray[0] = someone;
        testArray[1] = someoneB;
        testArray[2] = someoneC;

        Login testing = new Login(testArray);

        assertEquals(true, testing.validate("Hey", "There", true));
    }


    //Testing if the user isn't an admin and pass/ user aren't the same.

    @Test

    public void testValidateFalse(){

        UserType admin = UserType.ADMIN;
        UserType regular = UserType.REGULAR;

        User someone = new User("Hey", "There", admin);
        User someoneB = new User("Hey", "There", regular);
        User someoneC = new User("Hey", "Hey", admin);

        User[] testArray = new User[3];
        testArray[0] = someone;
        testArray[1] = someoneB;
        testArray[2] = someoneC;

        Login testing = new Login(testArray);

    
        assertEquals(false, testing.validate("Hey", "Blah blah", true));
    }

  
   
    //Testing if the user is regular, but the pass/user is correct.

    @Test

    public void testValidateReg(){

        UserType admin = UserType.ADMIN;
        UserType regular = UserType.REGULAR;

        User someone = new User("Hey", "There", admin);
        User someoneB = new User("Hey", "There", regular);
        User someoneC = new User("Hey", "Hey", admin);

        User[] testArray = new User[3];
        testArray[0] = someone;
        testArray[1] = someoneB;
        testArray[2] = someoneC;

        Login testing = new Login(testArray);

        assertEquals(true, testing.validate("Hey", "There", false));
    }

}
